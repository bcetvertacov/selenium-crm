﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Models.AffiliateProgram;
using CA_SELENIUM.Models.Deposit;
using OpenQA.Selenium;

namespace CA_SELENIUM.Models.GeneralUI
{
    public class SlideMenuModel
    {

        private string DashboardSelector = "sidebar-menu-dashboard";
        private string AccountSummarySelector = "sidebar-menu-profile";
        private string TradingAccountsSelector = "sidebar-menu-account";
        private string DepositAndWithdrawalsSelector = "sidebar-menu-fund";
        private string AffiliateProgramSelector = "sidebar-menu-affiliateProgram";
        private string ReportsSelector = "reports-item";
        private string DownloadCentreSelector = "sidebar-menu-download";
        private string DocumentationsSelector = "sidebar-menu-documentations";
        private string FaqHelpSelector = "sidebar-menu-faq";


        public IWebElement Dashboard { get; set; }        
        public IWebElement AccountSummary { get; set; }        
        public IWebElement TradingAccounts { get; set; }      
        public IWebElement DepositAndWithdrawals { get; set; }       
        public IWebElement AffiliateProgram { get; set; }       
        public IWebElement Reports { get; set; }       
        public IWebElement DownloadCentre { get; set; }
        public IWebElement Documentations { get; set; }
        public IWebElement FaqHelp { get; set; }

        public AffiliateProgramModel AffiliateDdl { get; set; }
        public DepositAndWithdrawalsModel DepositAndWithdrawalsDdl { get; set; }

        public SlideMenuModel(IWebDriver driver)
        {
            BaseTest.waitElemntToBeClickableById(driver, DashboardSelector, 5); //wait till page is loaded

            Dashboard = driver.FindElement(By.Id(DashboardSelector));
            AccountSummary = driver.FindElement(By.Id(AccountSummarySelector));
            TradingAccounts = driver.FindElement(By.Id(TradingAccountsSelector));
            DepositAndWithdrawals = driver.FindElement(By.Id(DepositAndWithdrawalsSelector));
            AffiliateProgram = driver.FindElement(By.Id(AffiliateProgramSelector));
            Reports = driver.FindElement(By.Id(ReportsSelector));
            DownloadCentre = driver.FindElement(By.Id(DownloadCentreSelector));
            Documentations = driver.FindElement(By.Id(DocumentationsSelector));
            FaqHelp = driver.FindElement(By.Id(FaqHelpSelector));

            AffiliateDdl = new AffiliateProgramModel(driver);
            DepositAndWithdrawalsDdl = new DepositAndWithdrawalsModel(driver);
                

        }


    }
}
