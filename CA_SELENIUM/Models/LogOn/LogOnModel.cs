﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CA_SELENIUM.Models.LogOn
{
    public class LogOnModel
    {

        public IWebElement Email { get; set; }
        public IWebElement Password { get; set; }
        public IWebElement LoginBtn { get; set; }

        public LogOnModel(IWebDriver driver)
        {
            Email = driver.FindElement(By.Id("UserName"));
            Password = driver.FindElement(By.Id("Password"));
            LoginBtn = driver.FindElement(By.Id("loginBtn"));
        }

        public LogOnModel()
        {
        }
    }

}
