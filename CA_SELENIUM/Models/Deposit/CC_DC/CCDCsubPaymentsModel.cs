﻿using CA_SELENIUM.Models.Deposit.CC_DC;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CA_SELENIUM.Models.Deposit
{
    public class CCDCsubPaymentsModel
    {

        private string SubPaymentsDdlSelector = "PaymentMethod";
        private IWebElement SubPaymentsDdl { get; set; }
        public SelectElement SubPaymentsMethodOptions { get; set; }
        
        public CurrencyDdlModel CurrencyDdl;
        public BankDdlModel BankDdll;
        public AmountTxBxModel AmountTxBox;

        public CCDCsubPaymentsModel(IWebDriver driver)
        {
            SubPaymentsDdl = driver.FindElement(By.Id(SubPaymentsDdlSelector));
            SubPaymentsMethodOptions = new SelectElement(SubPaymentsDdl);
        }

        public void initDynamicFields(IWebDriver driver)
        {

            CurrencyDdl = new CurrencyDdlModel(driver);
            BankDdll = new BankDdlModel(driver);
            AmountTxBox = new AmountTxBxModel(driver);
        }
    }
}
