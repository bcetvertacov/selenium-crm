﻿using CA_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

namespace CA_SELENIUM.Models.Deposit
{
    public class DepositFundsModel
    {

        private string PaymentMethodsRdBoxesSelector = "paymentmethods"; //className
        private string TradingAccountDdlSelector = "AccountId";

        private IWebElement TradingAccountDdl { get; set; }
        public ReadOnlyCollection<IWebElement> PaymentMethodsRdBoxes { get; set; }
        public SelectElement TradingAccountDdlOptions { get; set; }
        


        public DepositFundsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingAccountDdlSelector, 10);

            TradingAccountDdl = driver.FindElement(By.Id(TradingAccountDdlSelector));
            PaymentMethodsRdBoxes = driver.FindElements(By.ClassName(PaymentMethodsRdBoxesSelector));


            TradingAccountDdlOptions = new SelectElement(TradingAccountDdl);
        }

    }
}


