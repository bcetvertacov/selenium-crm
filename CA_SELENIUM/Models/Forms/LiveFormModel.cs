﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace CA_SELENIUM.Models.Forms
{
    public class LiveFormModel
    {
        //TODO clean code from comments

        private IWebElement TitleDdl { get; set; }      
        public IWebElement FirstName { get; set; }
        public IWebElement LastName { get; set; }
        public IWebElement Email { get; set; }
        private IWebElement TradingPlatformDdl { get; set; }
        private IWebElement AccountTypeDdl { get; set; }
        private IWebElement LeverageDdl { get; set; }
        public IWebElement TermsAndConditionsCheckBox { get; set; }
        public IWebElement Captcha { get; set; }

        public IWebElement SubmitBtn { get; set; }




        public SelectElement TitleDdlOptions { get; set; }
        public SelectElement TradingPlatformDdlOptions { get; set; }
        public SelectElement AccountTypeDdlOptions { get; set; }
        public SelectElement LeverageDdlOptions { get; set; }

        public LiveFormModel(IWebDriver driver)
        {
            TitleDdl = driver.FindElement(By.Id("TitleKey"));
            FirstName = driver.FindElement(By.Id("Firstname"));
            LastName = driver.FindElement(By.Id("Lastname"));
            Email = driver.FindElement(By.Id("Email"));
            TradingPlatformDdl = driver.FindElement(By.Id("TradingSystemKey"));
            AccountTypeDdl = driver.FindElement(By.Id("AccountTypeKey"));
            LeverageDdl = driver.FindElement(By.Id("LeverageKey"));
            TermsAndConditionsCheckBox = driver.FindElement(By.Id("HasAcceptedTerms"));
            Captcha = driver.FindElement(By.Id("CaptchaCode"));
            SubmitBtn = driver.FindElement(By.Id("wizzard-enable"));

            TitleDdlOptions = new SelectElement(TitleDdl);
            TradingPlatformDdlOptions = new SelectElement(TradingPlatformDdl);
            AccountTypeDdlOptions = new SelectElement(AccountTypeDdl);
            LeverageDdlOptions = new SelectElement(LeverageDdl);
        }
    }
}
