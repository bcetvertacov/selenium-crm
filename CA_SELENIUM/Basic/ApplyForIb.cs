﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Models.AffiliateProgram;
using CA_SELENIUM.Models.GeneralUI;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace CA_SELENIUM.Basic
{
    public class ApplyForIb
    {
        IWebDriver driver;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;
        private const string BASE_URL = BaseTest.BASE_URL_CA;

        private const string URL_PATH_TO_GET_1 = "/AffiliateProgram/ApplyForIb";

        private ApplyForIBFormModel applyForIbModel;
        private SlideMenuModel slideMenuModel;


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);

            driver.Manage().Window.Maximize();           
        }


        [Test]
        public void apply_for_ib()
        {
            var temp = new LogOnCa(driver);
            temp.log_on_ca();

            BaseTest.waitElemntVisibleById(driver, "sidebar-menu-dashboard", 7);
            slideMenuModel = new SlideMenuModel(driver);
            slideMenuModel.AffiliateProgram.Click();
        
            slideMenuModel.AffiliateDdl.ApplyForIB.Click();


            applyForIbModel = new ApplyForIBFormModel(driver);
            if (!applyForIbModel.TermsAndCondCkBox.Selected)
            {
                applyForIbModel.TermsAndCondCkBox.Click();
            }
            applyForIbModel.SubmitBtn.Click();

            var alerts = driver.FindElements(By.ClassName("alert"));
            foreach (var i in alerts)
            {
                if (i.GetCssValue("display") != "none")
                    throw new Exception("ALERT Message: /n" + i.Text);

            }
        }
        [TearDown]
        public void EndTest()
        {
            Thread.Sleep(2000);
            driver.Close();
            BaseTest.killChromeProcess();


        }

    }
}
