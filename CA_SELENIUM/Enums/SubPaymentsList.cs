﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_SELENIUM.Enums
{
    public class SubPaymentsList
    {
        public List<string> SubPaymentsEnum = new List<string>() {
       "SKRILL_CC"
,"SKRILL_DC"
,"PP"
,"WM"
,"NS"
,"SAFE_CC"
,"SKRILL_EW"
,"BANK"
,"CUP"
,"CUP_GA"
,"CUP_DP"
,"MANUAL"
,"FASA"
,"CUP_IPS"
,"DP_VISA"
,"DP_MASTER"
,"DP_JCB"
,"SCAN_WEIXIN"
,"SCAN_ALIPAY"
,"SCAN_TENPAY"
,"XIN_PAY"
,"PAYSEC"
,"OTHER_PAYMENT"
,"ZNY_ALIPAY"
,"ZNY_WECHAT"
,"UYIN"
,"SAND_PAY"
,"YZH_PAY"
,"STRIPE_ALIPAY"
,"STRIPE_CC"
,"IACCOUNT"
,"VIP_YZH"
,"COINBASE_BTC"
,"COINBASE_BCH"
,"COINBASE_ETH"
,"COINBASE_LTC"
,"HUANXUN"
,"FX_PAY"
,"COINSEPAY"
,"OMEN_PAY"
,"BPOTC"
,"HUNXNPAY_ALIPAY"
,"HUNXNPAY_CC"
,"SWIFTPASS_WECHAT"
        };
        public SubPaymentsList()
        {

        }
    }
}
