﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;

namespace CRM_SELENIUM.Basic
{
    public class LogOnToClientSearch
    {
        private IWebDriver driver;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;

        private const string BASE_URL = BaseTest.BASE_URL_CRM;
        private const string URL_PATH_TO_GET_1 = @"Account/LogOn/";

        private LogOnModel logOnModel;
        private SlideMenuModel slideMenu;

        [FindsBy(How = How.Id, Using = "Password")]
        public IWebElement Password;

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void Log_on_to_client_search()
        {
            driver.Url = BASE_URL + URL_PATH_TO_GET_1;
            logOnModel = new LogOnModel(driver);

            logOnModel.Email.SendKeys(BaseTest.ADMIN_EMAIL); //insert email
            logOnModel.Password.SendKeys(BaseTest.ADMIN_PASS); //insert pass
            logOnModel.LoginBtn.Click();
            
            slideMenu = new SlideMenuModel(driver);            
            slideMenu.Crm.Click();
            slideMenu.initCRMDdl(driver);
            slideMenu.CrmDdlOptions.ClientSearch.Click();
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }
    }
}
