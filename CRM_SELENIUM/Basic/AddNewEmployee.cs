﻿using CRM_SELENIUM.Models.Employee;
using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CRM_SELENIUM.Basic
{
    public  class AddNewEmployee
    {
        private IWebDriver driver { get; set; }
        private SlideMenuModel slideMenu { get; set; }
        private AddEmployeePersonalDetailsModel employeePersonalDetails { get; set; }
        private AddEmployeeDetailsModel employeeDetails { get; set; }

        private const string FIRST_NAME = "FNameEmployee";
        private const string LAST_NAME = "LNameEmployee";
        private const string TEST_EMAIL = "@yopmail.com";

        private delegate string RandomString(int lenght, string concat);
        RandomString randomString = BaseTest.randomString;

        private delegate string RandomNumbers(int lenght);
        RandomNumbers randomPhone = BaseTest.randomNumbers;

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void Add_new_employee()
        {
            string firstNameInsert = randomString(4,FIRST_NAME);
            string lastNameInsert = randomString(4, LAST_NAME);
            string emailInsert = lastNameInsert + TEST_EMAIL;
            string addressInsert = randomString(4,"ADDRESS");
            string phoneInsert = randomPhone(8);

            var logOn = new LogOnCRM(driver);
            logOn.log_on_crm();

            slideMenu = new SlideMenuModel(driver);
            slideMenu.Employee.Click();
            slideMenu.initEmployeeDdl(driver);
            slideMenu.EmployeeDdlOptions.AddNewEmployee.Click();

            employeePersonalDetails = new AddEmployeePersonalDetailsModel(driver);
            employeePersonalDetails.FirstName.SendKeys(firstNameInsert);
            employeePersonalDetails.LastName.SendKeys(lastNameInsert);
            employeePersonalDetails.CountryDdlOptions.SelectByIndex(1);
            employeePersonalDetails.Email.SendKeys(emailInsert);
            employeePersonalDetails.Address.SendKeys(addressInsert);
            employeePersonalDetails.Phone.SendKeys(phoneInsert);
            employeePersonalDetails.ContinueBtn.Click();

            employeeDetails = new AddEmployeeDetailsModel(driver);
            employeeDetails.DepartmentDdlOptions.SelectByText("IT");
            employeeDetails.RoleDdlOptions.SelectByValue("EMPLOYEE.BACKOFFICE");

            employeeDetails.SubmitBtn.Click();

            BaseTest.waitElemntVisibleByXPath(driver, "//*[contains(@class, 'portlet box purple-studio')]", 30); // TODO adjust || check if test fails

        }
        [TearDown]
        public void endTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }
    }
}
