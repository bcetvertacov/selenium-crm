﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using CRM_SELENIUM.Models.SearchesModels.ClientSearchModels;
using CRM_SELENIUM.Models.UserPageModels.UserPageTabs;
using CRM_SELENIUM.Models.UserPageModels.TradingAccountsModels;

namespace CRM_SELENIUM.Basic
{
    public class BoCreateTA
    {
        private IWebDriver driver { get; set; }
        private LogOnCRM logOn { get; set; }
        private SlideMenuModel slideMenu { get; set; }
        private ClientSearchExtraFiltersModel extraFields { get; set; }
        private ClientSearchFiltersModel filters { get; set; }
        private ClientSearchDataTableModel dataTable { get; set; }
        private ClientPageTabsModel userTabs { get; set; }
        private TradingAccountsPageModel tasPage { get; set; }
        private CreateTradingAccountFormModel taFrom { get; set; }

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void Bo_create_new_TA()
        {
            logOn = new LogOnCRM(driver);
            logOn.log_on_crm();

            slideMenu = new SlideMenuModel(driver);
            slideMenu.Crm.Click();
            slideMenu.initCRMDdl(driver);
            slideMenu.CrmDdlOptions.ClientSearch.Click();

            extraFields = new ClientSearchExtraFiltersModel(driver);
            extraFields.CollapseExtend.Click();
            extraFields.RoleDdlOptions.SelectByValue("LIVE");

            filters = new ClientSearchFiltersModel(driver);
            filters.SearchBtn.Click();

            dataTable = new ClientSearchDataTableModel(driver);
            dataTable.ClientsIds[0].Click();

            userTabs = new ClientPageTabsModel(driver);
            userTabs.TradingAccounts.Click();

            tasPage = new TradingAccountsPageModel(driver);
            tasPage.CreateTradingAccountBtn.Click();

            taFrom = new CreateTradingAccountFormModel(driver);
            taFrom.TradingSystemDdlOptions.SelectByValue("MT4");
            taFrom.InitEnvAndAccType(driver);
            taFrom.EnvironmentDdlOptions.SelectByValue("LIVE");
            taFrom.InitTradingPlatform(driver);
            taFrom.TradingPlatformDdlOptions.SelectByValue("MT4_LIVE");
            taFrom.AccountTypeDdlOptions.SelectByIndex(1);
            taFrom.AccountBaseCurrencyDdlOptions.SelectByValue("USD");
            taFrom.PlatformGroupDdl.Click();
            taFrom.InitPlatformGroupOptions(driver);
            taFrom.PlatformGroupDdlOptions[1].Click();
            taFrom.InitLeverage(driver);
            taFrom.LeverageDdlOptions.SelectByIndex(1);
            taFrom.SubmitBtn.Click();

            BaseTest.waitElemntToDisapearById(driver, "submit_form", 15);
        }

       [TearDown]
        public void EndTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }

    }
}

