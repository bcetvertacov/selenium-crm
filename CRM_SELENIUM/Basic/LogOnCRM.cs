﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace CRM_SELENIUM.Basic
{
    [TestFixture]
    public class LogOnCRM
    {
        IWebDriver driver;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;
        private const string BASE_URL = BaseTest.BASE_URL_CRM;

        private const string URL_PATH_TO_GET_1 = "Account/LogOn";

        private LogOnModel logOnModel;

      //  public readonly ILog logger = LogManager.GetLogger(typeof(LogOnCRM));

        public LogOnCRM(IWebDriver driver)
        {
            this.driver = driver;

        }
        public LogOnCRM()
        {
            //ConstantsToUse.initLogger();

            //logger.Debug("asd");
        }

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void log_on_crm()
        {
            driver.Url = BASE_URL + URL_PATH_TO_GET_1;
            //logger.Debug(driver.Url+ "URL");

            logOnModel = new LogOnModel(driver);
            logOnModel.Email.SendKeys(BaseTest.ADMIN_EMAIL);
            logOnModel.Password.SendKeys(BaseTest.ADMIN_PASS);
            logOnModel.LoginBtn.Click();

            Thread.Sleep(1000);

     //       driver.FindElement(By.XPath(BaseTest.XPATH_TO_EN)).Click();
            //logger.Debug(BaseTest.XPATH_TO_EN + "URL");
           // return driver;
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();


        }
    }
}