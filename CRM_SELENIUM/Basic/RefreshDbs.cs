﻿using CRM_SELENIUM.Models;
using CRM_SELENIUM.BaseModule;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CRM_SELENIUM.Basic
{
    public class RefreshDbs
    {
        private IWebDriver driver;
        private SlideMenuModel slideMenu;
        private RefreshDbsViewModel refreshDbsView;


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }
        [Test]
        public void Bo_refresh_dbs()
            {
            var temp = new LogOnCRM(driver);
              temp.log_on_crm();
            slideMenu = new SlideMenuModel(driver);
            slideMenu.Configurations.Click();
            slideMenu.initConfigurationDdl(driver);
            slideMenu.ConfigurationsDdlOptions.DataBase.Click();

            refreshDbsView = new RefreshDbsViewModel(driver);
            refreshDbsView.RefreshBoDbBtn.Click();

            var testIsPassed = refreshDbsView.getSuccessAlertBox(driver);
            if (testIsPassed)
            {
                driver.Navigate().Refresh();
                refreshDbsView = new RefreshDbsViewModel(driver);
                refreshDbsView.RefreshCaDbBtn.Click();
                testIsPassed = refreshDbsView.getSuccessAlertBox(driver);
                if (!testIsPassed)
                { Assert.Fail(); }
            }
            else { Assert.Fail(); }


            }
        [TearDown]
        public void endTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }
    }
}
