﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models
{
    public class RefreshDbsViewModel
    {
        private readonly string RefreshBoDbBtnSelector = "//a[contains(@class, 'btn btn-md green ladda-button')]";
        private readonly string RefreshCaDbBtnSelector = "//a[contains(@class, 'btn btn-md blue  ladda-button')]";

        private readonly string SuccessAlertMsgClass = "alert alert-success";
        private readonly string SuccessAlertMsgId = "UpdateStatus";

        private readonly string SuccessAlertMsgCAText = "Client Area Database was successfully updated";
        private readonly string SuccessAlertMsgBoText = "Back Office Database was successfully updated";

        public IWebElement RefreshBoDbBtn { get; set; }
        public IWebElement RefreshCaDbBtn { get; set; }
        public RefreshDbsViewModel(IWebDriver driver)
        {
            RefreshBoDbBtn = driver.FindElement(By.XPath(RefreshBoDbBtnSelector));
            RefreshCaDbBtn = driver.FindElement(By.XPath(RefreshCaDbBtnSelector));
        }

        public bool getSuccessAlertBox(IWebDriver driver)
        {
            try
            {
                BaseTest.waitElemntVisibleById(driver, SuccessAlertMsgId,60);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
