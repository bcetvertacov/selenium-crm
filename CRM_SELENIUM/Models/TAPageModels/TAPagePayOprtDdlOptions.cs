﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.TAPageModels
{
   public class TAPagePayOprtDdlOptions
    {
        private readonly string DepositSelector =      "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[*]/ul/li[1]";
        private readonly string WithdrawSelector =     "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[*]/ul/li[2]";
        private readonly string CommissionSelector =   "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[*]/ul/li[3]";
        private readonly string FeeSelector =          "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[*]/ul/li[4]";
        private readonly string CompensasionSelector = "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[*]/ul/li[5]";

        public IWebElement Deposit { get; set; }
        public IWebElement Withdraw { get; set; }
        public IWebElement Commission { get; set; }
        public IWebElement Fee { get; set; }
        public IWebElement Compensation { get; set; }


        public TAPagePayOprtDdlOptions(IWebDriver driver)
        {
            Deposit = driver.FindElement(By.XPath(DepositSelector));
            Withdraw = driver.FindElement(By.XPath(WithdrawSelector));
            Commission = driver.FindElement(By.XPath(CommissionSelector));
            Fee = driver.FindElement(By.XPath(FeeSelector));
            Compensation = driver.FindElement(By.XPath(CompensasionSelector));



        }
    }
}
