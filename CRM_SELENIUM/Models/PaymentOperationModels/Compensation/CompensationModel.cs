﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CRM_SELENIUM.Models.PaymentOperationModels.Compensation
{
    public class CompensationModel
    {
        private readonly string TradingAccountDdlSelector = "AccountNumber";
        private readonly string AmountSelector = "Amount";
        private readonly string CommentSelector = "Comments";
        private readonly string SubmitBtnSelector = "submitBtn";

        public IWebElement TradingAccountDdl { get; set; }
        public IWebElement Amount { get; set; }
        public IWebElement Comment { get; set; } 
        public IWebElement SubmitBtn { get; set; }

        public SelectElement TradingAccountDdlOptions { get; set; }

        public CompensationModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingAccountDdlSelector, 8);

            TradingAccountDdl = driver.FindElement(By.Id(TradingAccountDdlSelector));            
            Amount = driver.FindElement(By.Id(AmountSelector));
            Comment = driver.FindElement(By.Id(CommentSelector));
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));

            TradingAccountDdlOptions = new SelectElement(TradingAccountDdl);            
        }
    }
}
