﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CRM_SELENIUM.Models.MenuModels
{
    public class SlideMenuEmployeeModel
    {
        private readonly string ManageEmployeesSelector = "[href*='/CrmUser/SearchEmployees']";
        private readonly string ManageDepartmentsSelector = "[href*='/Employee/ManageDepartments']";
        private readonly string AddNewEmployeeSelector = "[href*='/Employee/CreateEmployee']";



        public IWebElement ManageEmployees { get; set; }
        public IWebElement ManageDepartments { get; set; }
        public IWebElement AddNewEmployee { get; set; }

        public SlideMenuEmployeeModel(IWebDriver driver)
        {
            Thread.Sleep(1000);
            
            ManageEmployees = driver.FindElement(By.CssSelector(ManageEmployeesSelector));
            ManageDepartments = driver.FindElement(By.CssSelector(ManageDepartmentsSelector));
            AddNewEmployee = driver.FindElement(By.CssSelector(AddNewEmployeeSelector));
        }
    }
}
