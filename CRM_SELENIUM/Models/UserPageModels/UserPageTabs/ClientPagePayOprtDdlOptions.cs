﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.UserPageModels.UserPageTabs
{
   public class ClientPagePayOprtDdlOptions
    {
        private readonly string DepositSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/ul/li[1]";
        private readonly string WithdrawSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/ul/li[2]";
        private readonly string CommissionSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/ul/li[3]";
        private readonly string FeeSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/ul/li[4]";
        private readonly string CompensasionSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/ul/li[5]";
        
        //*[@id="selectTab"]/li[1]/ul/li[3]/ul/li[2]
        public IWebElement Deposit { get; set; }
        public IWebElement Withdraw { get; set; }
        public IWebElement Commission { get; set; }
        public IWebElement Fee { get; set; }
        public IWebElement Compensation { get; set; }


        public ClientPagePayOprtDdlOptions(IWebDriver driver)
        {
            Deposit = driver.FindElement(By.XPath(DepositSelector));
            Withdraw = driver.FindElement(By.XPath(WithdrawSelector));
            Commission = driver.FindElement(By.XPath(CommissionSelector));
            Fee = driver.FindElement(By.XPath(FeeSelector));
            Compensation = driver.FindElement(By.XPath(CompensasionSelector));
        }
    }
}
