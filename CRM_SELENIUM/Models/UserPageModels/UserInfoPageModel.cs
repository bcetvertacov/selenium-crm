﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models.UserPageModels.UserPageTabs;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models
{
    public class TabsUserInfoPageModel
    {
        private readonly string ActionsDdlSelector = "//*[@id='selectTab']/li[1]/a";
        private readonly string MainInfoSelector = "";
        private readonly string KYCSelector = "";
        private readonly string TradingAccountsSelector = "";
        private readonly string ClientDocumentsSelector = "";
        private readonly string PermissionsSelector = "";
        private readonly string HistoryTimelineSelector = "";
        private readonly string TradeHistorySelector = "";
        private readonly string OpenPositionsSelector = "";
        private readonly string ChartsReportsSelector = "";

        public IWebElement ActionsDdl { get; set; }
        public  ClientPageActionsDdlOptionsModel ActionTabOptions { get; set; }
        public TabsUserInfoPageModel(IWebDriver driver)
        {
            BaseTest.waitElemntToBeClickableByXPath(driver, ActionsDdlSelector, 8);
            ActionsDdl = driver.FindElement(By.XPath(ActionsDdlSelector));

          // NavigationTabs = driver.FindElements(By.XPath("//*[@id='selectTab']/li[*]/a"));
          // ActionsDdlOptions = driver.FindElements(By.XPath("//*[@id='selectTab']/li[1]/a"));
          // *[@id="selectTab"]/li[1]/a
          // ActionsDdlOptionsList.Add(ActnInternalTransfer);
          // ActionsDdlOptionsList.

        }

        public void InitActionTabOptions(IWebDriver driver)
        {
            ActionTabOptions = new ClientPageActionsDdlOptionsModel(driver);
        }


    }
}
