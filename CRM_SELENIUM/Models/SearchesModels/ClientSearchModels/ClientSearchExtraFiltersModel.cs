﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CRM_SELENIUM.Models.SearchesModels.ClientSearchModels
{
    public class ClientSearchExtraFiltersModel
    {
        private readonly string RoleDdlSelector = "Role";
        private readonly string AccountManagerDdlSelector = "AccountManager";
        private readonly string CountryDdlSelector = "Country";
        private readonly string CityTxbSelector = "City";
        private readonly string QQTxbSelector = "QQ";
        private readonly string IncludeArchivedClientsToggleSelector = "SearchInArchive";
        private readonly string CollapseExtendSelector = "//*[@class='tools']";

        public IWebElement RoleDdl { get; set; }
        public IWebElement AccountManagerDdl { get; set; }
        public IWebElement CountryDdl { get; set; }
        public IWebElement CityTxb { get; set; }
        public IWebElement QQTxb { get; set; }
        public IWebElement IncludeArchivedClientsToggle { get; set; }
        public IWebElement CollapseExtend { get; set; }
        public SelectElement RoleDdlOptions { get; set; }
        public SelectElement AccountManagerDdlOptions { get; set; }
        public SelectElement CountryDdlOptions { get; set; }



        public ClientSearchExtraFiltersModel(IWebDriver driver)
        {
            BaseTest.waitElemntToBeClickableByXPath(driver, CollapseExtendSelector, 8);

            RoleDdl = driver.FindElement(By.Id(RoleDdlSelector));
            AccountManagerDdl = driver.FindElement(By.Id(AccountManagerDdlSelector));
            CountryDdl = driver.FindElement(By.Id(CountryDdlSelector));
            CityTxb = driver.FindElement(By.Id(CityTxbSelector));
            QQTxb = driver.FindElement(By.Id(QQTxbSelector));
            IncludeArchivedClientsToggle = driver.FindElement(By.Id(IncludeArchivedClientsToggleSelector));
            CollapseExtend = driver.FindElement(By.XPath(CollapseExtendSelector));

            RoleDdlOptions = new SelectElement(RoleDdl);
            AccountManagerDdlOptions = new SelectElement(AccountManagerDdl);
            CountryDdlOptions = new SelectElement(CountryDdl);
        }

        public ClientSearchExtraFiltersModel()
        {
        }
    }
}
