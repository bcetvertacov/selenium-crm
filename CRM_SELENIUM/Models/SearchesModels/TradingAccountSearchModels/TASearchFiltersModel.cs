﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels.TradingAccountSearchBoundariesFiltersModels;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels
{
   public class TASearchFiltersModel
    {
        private readonly string SearchBtnSelector = "SearchTradingAccounts_searchAction";
        private readonly string ResetBtnSelector = "SearchTradingAccounts_resetTableValues";

        public IWebElement SearchBtn { get; set; }
        public IWebElement ResetBtn { get; set; }
        public IWebElement TradingAccount { get; set; }



        public BalanceBoundariesFilterModel Balance { get; set; }

        public TASearchFiltersModel(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearByClassName(driver, "blockOverlay", 5);

            SearchBtn = driver.FindElement(By.Id(SearchBtnSelector));
            ResetBtn = driver.FindElement(By.Id(ResetBtnSelector));
            Balance = new BalanceBoundariesFilterModel(driver);
        }
    }
}
