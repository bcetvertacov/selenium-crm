﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels
{
    public class TASearchDataTableModel
    {
        private readonly string TAsNumbersSelector = "//*[@id='SearchTradingAccounts']/tbody/tr[*]/td[4]/a";

        public ReadOnlyCollection<IWebElement> TAsNumbers { get; set; }

        public TASearchDataTableModel(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearByClassName(driver, "blockOverlay", 5);
            TAsNumbers = driver.FindElements(By.XPath(TAsNumbersSelector));
        }


    }
}
